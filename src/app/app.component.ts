import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  private _phoneNumber
  set phoneNumber(val) {
    if (!val) {
      this.combinations = []
    }
    this._phoneNumber = val
  }

  pageIndex: number = 0;
  pageSize: number = 10;
  lowValue: number = 0;
  highValue: number = 10;

  phoneKeypad = {
    "0": ["0"],
    "1": ["1"],
    "2": ["a", "b", "c"],
    "3": ["d", "e", "f"],
    "4": ["g", "h", "i"],
    "5": ["j", "k", "l"],
    "6": ["m", "n", "o"],
    "7": ["p", "q", "r", "s"],
    "8": ["t", "u", "v"],
    "9": ["w", "x", "y", "z"]
  };
  combinations = []

  onSubmit() {

    console.log('number', this._phoneNumber)
    this.combinations = [''];
    if (this._phoneNumber.length === 0) return [];

    for (var i = 0; i < this._phoneNumber.length; i++) {
      var digit = this._phoneNumber[i];
      var letters = this.phoneKeypad[digit];
      var tempArray = [];
    if (letters === undefined) continue;

      for (var j = 0; j < letters.length; j++) {
        var letterToAdd = letters[j];
        for (var k = 0; k < this.combinations.length; k++) {
          var combination = this.combinations[k];
          tempArray.push(combination + letterToAdd);
        }
      }
      this.combinations = tempArray;
    }
  }

  getNextPage(event) {
    if (event.pageIndex === this.pageIndex + 1) {
      this.lowValue = this.lowValue + this.pageSize;
      this.highValue = this.highValue + this.pageSize;
    }
    else if (event.pageIndex === this.pageIndex - 1) {
      this.lowValue = this.lowValue - this.pageSize;
      this.highValue = this.highValue - this.pageSize;
    }
    this.pageIndex = event.pageIndex;
  }
}
